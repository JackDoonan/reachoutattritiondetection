
from sklearn.cross_validation import cross_val_score


import os
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.ensemble import RandomForestClassifier
import pandas as pd
import numpy as np
import json
from pprint import pprint

with open('data.json') as data_file:    
    data = json.load(data_file)

pprint(data)