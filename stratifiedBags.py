'''
Created on May 3, 2017

@author: jack
'''
from sklearn.cross_validation import cross_val_score
'''
Created on Apr 9, 2017

@author: jack
'''

import os
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.ensemble import RandomForestClassifier
import pandas as pd
import numpy as np

train = pd.read_csv("C://Users/Jack/My Documents/THESIS/stratifiedTrain.tsv", header=0, \
                    delimiter="\t", quoting=3)


from bs4 import BeautifulSoup 

import re

import nltk

from nltk.corpus import stopwords # Import the stop word list

def post_to_words( raw_post ):
    # Function to convert a raw post to a string of words
    # The input is a single string (a raw post), and 
    # the output is a single string (a preprocessed post)
    #
    # 1. Remove HTML
    post_text = BeautifulSoup(raw_post).get_text() 
    #
    # 2. Remove non-letters        
    letters_only = re.sub("[^a-zA-Z]", " ", post_text) 
    #
    # 3. Convert to lower case, split into individual words
    words = letters_only.lower().split()                             
    #
    # 4. In Python, searching a set is much faster than searching
    #   a list, so convert the stop words to a set
    stops = set(stopwords.words("english"))                  
    # 
    # 5. Remove stop words
    meaningful_words = [w for w in words if not w in stops]   
    
    #
    # 6. Join the words back into one string separated by space, 
    # and return the result.
    return( " ".join( meaningful_words ))   


num_posts = train["post"].size

# Initialize an empty list to hold the clean reviews
clean_train_posts = []

# Loop over each review; create an index i that goes from 0 to the length
# of the movie review list 
for i in range( 0, num_posts ):
    # Call our function for each one, and add the result to the list of
    # clean reviews
    print(post_to_words( train["post"][i] ))
    
    clean_train_posts.append( post_to_words( train["post"][i] ) )
    print(i)
print(clean_train_posts[100])

from sklearn.feature_extraction.text import CountVectorizer

# Initialize the "CountVectorizer" object, which is scikit-learn's
# bag of words tool.  
vectorizer = CountVectorizer(analyzer = "word",   \
                             tokenizer = None,    \
                             preprocessor = None, \
                             stop_words = None,   \
                             max_features = 5000) 

# fit_transform() does two functions: First, it fits the model
# and learns the vocabulary; second, it transforms our training data
# into feature vectors. The input to fit_transform should be a list of 
# strings.
train_data_features = vectorizer.fit_transform(clean_train_posts)

# Numpy arrays are easy to work with, so convert the result to an 
# array
train_data_features = train_data_features.toarray()

print(train_data_features.shape)


print("Training the random forest...")
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier

# Initialize a Random Forest classifier with 100 trees
forest = RandomForestClassifier(n_estimators = 100) 
kn =  KNeighborsClassifier(3)
sv = SVC(kernel="linear", C=0.025)
dtree = DecisionTreeClassifier(max_depth=5)
myLittleClassifier =   MLPClassifier(alpha=1)
GNB = GaussianNB()

# Fit the forest to the training set, using the bag of words as 
# features and the sentiment labels as the response variable
#
# This may take a few minutes to run
#forest = forest.fit( train_data_features, train["tag"] )
forest = GNB.fit( train_data_features, train["tag"] )


from sklearn import datasets

iris = datasets.load_iris()
print(iris.data.shape)
print(train.shape)
clf = forest
targetResult = train["tag"]

accscores = cross_val_score(clf, train_data_features, targetResult,cv = 10,scoring='accuracy')

print(accscores)
recscores = cross_val_score(clf, train_data_features, targetResult,cv = 10,scoring='recall')

print(recscores)

# Use pandas to write the comma-separated output file
#output.to_csv( "Bag_of_Words_model.csv", index=False, quoting=3 )