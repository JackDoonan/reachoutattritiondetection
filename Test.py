'''
Created on Apr 9, 2017

@author: jack
'''

import os
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.ensemble import RandomForestClassifier
import pandas as pd
import numpy as np

train = pd.read_csv("C://Users/Jack/My Documents/THESIS/TRAINDATA1.tsv", header=0, \
                    delimiter="\t", quoting=3)

print(train["post"][0])

from bs4 import BeautifulSoup 

example1 = BeautifulSoup(train["post"][0])  

print(train["post"][0])
print(example1.get_text())

import re
# Use regular expressions to do a find-and-replace
letters_only = re.sub("[^a-zA-Z]",           # The pattern to search for
                      " ",                   # The pattern to replace it with
                      example1.get_text() )  # The text to search
print(letters_only)

lower_case = letters_only.lower()        # Convert to lower case
words = lower_case.split()               # Split into words

import nltk

from nltk.corpus import stopwords # Import the stop word list
print(stopwords.words("english")) 

words = [w for w in words if not w in stopwords.words("english")]
print(words)


def post_to_words( raw_post ):
    # Function to convert a raw post to a string of words
    # The input is a single string (a raw post), and 
    # the output is a single string (a preprocessed post)
    #
    # 1. Remove HTML
    post_text = BeautifulSoup(raw_post).get_text() 
    #
    # 2. Remove non-letters        
    letters_only = re.sub("[^a-zA-Z]", " ", post_text) 
    #
    # 3. Convert to lower case, split into individual words
    words = letters_only.lower().split()                             
    #
    # 4. In Python, searching a set is much faster than searching
    #   a list, so convert the stop words to a set
    stops = set(stopwords.words("english"))                  
    # 
    # 5. Remove stop words
    meaningful_words = [w for w in words if not w in stops]   
    #
    # 6. Join the words back into one string separated by space, 
    # and return the result.
    return( " ".join( meaningful_words ))   



clean_post = post_to_words( train["post"][0] )
print(clean_post)


num_posts = train["post"].size

# Initialize an empty list to hold the clean reviews
clean_train_posts = []

# Loop over each review; create an index i that goes from 0 to the length
# of the movie review list 
for i in range( 0, num_posts ):
    # Call our function for each one, and add the result to the list of
    # clean reviews
    clean_train_posts.append( post_to_words( train["post"][i] ) )
    print(i)
print(clean_train_posts[100])

from sklearn.feature_extraction.text import CountVectorizer

# Initialize the "CountVectorizer" object, which is scikit-learn's
# bag of words tool.  
vectorizer = CountVectorizer(analyzer = "word",   \
                             tokenizer = None,    \
                             preprocessor = None, \
                             stop_words = None,   \
                             max_features = 5000) 

# fit_transform() does two functions: First, it fits the model
# and learns the vocabulary; second, it transforms our training data
# into feature vectors. The input to fit_transform should be a list of 
# strings.
train_data_features = vectorizer.fit_transform(clean_train_posts)

# Numpy arrays are easy to work with, so convert the result to an 
# array
train_data_features = train_data_features.toarray()

print(train_data_features.shape)


print("Training the random forest...")
from sklearn.ensemble import RandomForestClassifier

# Initialize a Random Forest classifier with 100 trees
forest = RandomForestClassifier(n_estimators = 100) 

# Fit the forest to the training set, using the bag of words as 
# features and the sentiment labels as the response variable
#
# This may take a few minutes to run
forest = forest.fit( train_data_features, train["tag"] )

test = pd.read_csv("C://Users/Jack/My Documents/THESIS/TESTDATA1.tsv", header=0, delimiter="\t", \
                   quoting=3 )
num_posts = len(test["post"])
clean_test_posts = [] 

print("Cleaning and parsing the test set movie reviews...\n")
for i in range(0,num_posts):
    if( (i+1) % 1000 == 0 ):
        print("Review %d of %d\n" % (i+1, num_posts))
    clean_post = post_to_words( test["post"][i] )
    clean_test_posts.append( clean_post )

# Get a bag of words for the test set, and convert to a numpy array
test_data_features = vectorizer.transform(clean_test_posts)
test_data_features = test_data_features.toarray()

# Use the random forest to make sentiment label predictions
result = forest.predict(test_data_features)

# Copy the results to a pandas dataframe with an "id" column and
# a "sentiment" column
output = pd.DataFrame( data={"tag":result} )

# Use pandas to write the comma-separated output file
output.to_csv( "Bag_of_Words_model.csv", index=False, quoting=3 )